#!/bin/bash
declare -a  trainConditions=("hybrid" "exact" "hard")
declare -a  maybeConditions=("with_maybes_in_train" "with_maybes_in_train" "no_maybes_in_train")

for notBullyWeight in 1 5
do
for notTiedWeight in 1 5
do
for otherPrior in 1 5
do
for snToBullyWeight in 1 5
do
for silenceToBullyWeight in 1 5
do
for nameToBullyWeight in 1 5
do
for threatToBullyWeight in 1 5
do
for snToBullyWeight in 1 5
do
for teasingToBullyWeight in 1 5
do
for wordToCatWeight in 1 5
do
for sentWeight in 1 5
do
for wvSimWeight in 1 5
do
for cooccurWeight in 1 5
do
for assocWeight in 1 5
do
for sClusterWeight in 1 5
do
for potentialWordToCatWeight in 1 5
do
for tweetSimWeight in 1 5
do
for proNounWeight in 1 5
do
for roleWeightSn in 1 5
do
for roleWeightName in 1 5
do
for roleWeightThreat in 1 5
do
for roleWeightSilence in 1 5
do
for roleVictimToBullyWeightSilence in 1 5
do
for roleVictimToBullyWeightThreat in 1 5
do
for roleVictimToBullyWeightName in 1 5
do
for roleVictimToBullyWeightSn in 1 5
do
for otherNeutralMentionsWeight in 1 5
do
for otherNeutralWeight in 1 5
do
for teasingNeutralMentionsWeight in 1 5
do
for teasingNeutralWeight in 1 5
do
for negBullyWeight in 1 5
do
for posNeutralWeight in 1 5
do
for symmetricRoleWeight in 1 5
do
for followsRTieWeight in 1 5
do
for inferRTieWeight in 1 5
do
for teasingRTieWeight in 1 5
do
for otherRTieWeight in 1 5
do
for sentRTieWeight in 1 5
do
for tieNotBullyWeight in 1 5
do
for tieThreatWeight in 1 5
do
for tieSnWeight in 1 5
do
for tieNameWeight in 1 5
do
for tieSilenceWeight in 1 5
do
for tieOtherWeight in 1 5
do
for tieTeasingWeight in 1 5
do
for normWeight in 1 5
do
for popBullyWeight in 1 5
do
for popVictimWeight in 1 5
do
for gangUpWeight in 1 5
do
mvn compile && java -Xmx30g -cp ./target/classes:`cat classpath.out` sociolinguistic.SocioLinguistic  hybrid with_maybes_in_train $notBullyWeight $notTiedWeight $otherPrior $snToBullyWeight $silenceToBullyWeight $nameToBullyWeight $threatToBullyWeight $snToBullyWeight $teasingToBullyWeight $wordToCatWeight $sentWeight $wvSimWeight $cooccurWeight $assocWeight $sClusterWeight $potentialWordToCatWeight $tweetSimWeight $proNounWeight $roleWeightSn $roleWeightName $roleWeightThreat $roleWeightSilence $roleVictimToBullyWeightSilence $roleVictimToBullyWeightThreat $roleVictimToBullyWeightName $roleVictimToBullyWeightSn $otherNeutralMentionsWeight $otherNeutralWeight $teasingNeutralMentionsWeight $teasingNeutralWeight $negBullyWeight $posNeutralWeight $symmetricRoleWeight $followsRTieWeight $inferRTieWeight $teasingRTieWeight $otherRTieWeight $sentRTieWeight $tieNotBullyWeight $tieThreatWeight $tieSnWeight $tieNameWeight $tieSilenceWeight $tieOtherWeight $tieTeasingWeight $normWeight $popBullyWeight $popVictimWeight $gangUpWeight
exit
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done
done