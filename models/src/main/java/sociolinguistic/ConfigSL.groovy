package sociolinguistic;

import org.linqs.psl.application.inference.MPEInference;
import org.linqs.psl.config.ConfigBundle;
import org.linqs.psl.config.ConfigManager;
import org.linqs.psl.database.Database;
import org.linqs.psl.database.DatabasePopulator;
import org.linqs.psl.database.DataStore;
import org.linqs.psl.database.Partition;
import org.linqs.psl.database.Queries;
import org.linqs.psl.database.ReadOnlyDatabase;
import org.linqs.psl.database.loading.Inserter;
import org.linqs.psl.database.rdbms.driver.H2DatabaseDriver;
import org.linqs.psl.database.rdbms.driver.H2DatabaseDriver.Type;
import org.linqs.psl.database.rdbms.RDBMSDataStore;
import org.linqs.psl.groovy.PSLModel;
import org.linqs.psl.model.atom.Atom;
import org.linqs.psl.model.predicate.StandardPredicate;
import org.linqs.psl.model.term.ConstantType;




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import groovy.time.TimeCategory;
import java.nio.file.Paths;
import groovy.json.JsonSlurper;
/**

 */

	/**
	 * Class for config variables
	 */
	public  class ConfigSL {
		public ConfigBundle cb;

		public String experimentName;
		public String dbPath;
		public String dataPath;
		public String outputPath;

        public Map weightMap=[:];
        public List apps = [];
		public boolean sqPotentials = true;
        public int vocabSize; 
        public String fold; 
        public String dataset;
        public String trainCondition;
        public String maybeCondition;
        
        
        public int notBullyWeight, notTiedWeight, otherPrior,  silenceToBullyWeight,                 nameToBullyWeight, threatToBullyWeight, snToBullyWeight, teasingToBullyWeight, wordToCatWeight,               sentWeight, wvSimWeight, cooccurWeight, assocWeight, sClusterWeight, potentialWordToCatWeight,               tweetSimWeight, roleWeightSn, roleWeightName, roleWeightThreat, roleWeightSilence,                           roleVictimToBullyWeightSilence, roleVictimToBullyWeightThreat, roleVictimToBullyWeightName,                   roleVictimToBullyWeightSn, symmetricRoleWeight, normWeight;
        
        public int tieThreatWeight;
        public int tieSnWeight;
        public int tieNameWeight;
        public int tieSilenceWeight;
        public int tieOtherWeight;
        public int tieTeasingWeight;
        
        
        public int gangUpWeight, popVictimWeight,popBullyWeight, tieNotBullyWeight, sentRTieWeight,                   otherRTieWeight, teasingRTieWeight, inferRTieWeight, followsRTieWeight, posNeutralWeight,                     negBullyWeight,teasingNeutralWeight, teasingNeutralMentionsWeight, otherNeutralWeight,                       otherNeutralMentionsWeight,proNounWeight;
        
        
        
		public ConfigSL(ConfigBundle cb) {
			this.cb = cb;
  

            //loadWeightsFromFile();
            
            println this.sqPotentials;
            //this.experimentName = cb.getString('experiment.name', 'interval');
            this.dbPath = cb.getString('experiment.dbpath', '/tmp');
            this.fold = cb.getString('experiment.fold','');

            this.dataset = cb.getString('experiment.dataset','');
        
	        this.dataPath = '../data/input/';
            this.outputPath =  '../data/output/';
            
            
            this.trainCondition =  cb.getString('experiment.trainCondition','');
            this.maybeCondition =  cb.getString('experiment.maybeCondition','');
            
            this.notBullyWeight = Integer.parseInt(cb.getString('experiment.notBullyWeight',''));
            
            
            
      
            this.notTiedWeight = Integer.parseInt(cb.getString('experiment.notTiedWeight',''));
            this.otherPrior = Integer.parseInt(cb.getString('experiment.otherPrior',''));

            this.silenceToBullyWeight = Integer.parseInt(cb.getString('experiment.silenceToBullyWeight',''));
            this.nameToBullyWeight = Integer.parseInt(cb.getString('experiment.nameToBullyWeight',''));
            this.threatToBullyWeight = Integer.parseInt(cb.getString('experiment.threatToBullyWeight',''));
            this.snToBullyWeight = Integer.parseInt(cb.getString('experiment.snToBullyWeight',''));
            this.teasingToBullyWeight = Integer.parseInt(cb.getString('experiment.teasingToBullyWeight',''));
            this.wordToCatWeight = Integer.parseInt(cb.getString('experiment.wordToCatWeight',''));
            this.sentWeight = Integer.parseInt(cb.getString('experiment.sentWeight',''));
            this.wvSimWeight= Integer.parseInt(cb.getString('experiment.wvSimWeight',''));
            this.cooccurWeight= Integer.parseInt(cb.getString('experiment.cooccurWeight',''));
            this.assocWeight= Integer.parseInt(cb.getString('experiment.assocWeight',''));
            this.sClusterWeight= Integer.parseInt(cb.getString('experiment.sClusterWeight',''));
            this.potentialWordToCatWeight= Integer.parseInt(cb.getString('experiment.potentialWordToCatWeight',''));
            this.tweetSimWeight= Integer.parseInt(cb.getString('experiment.tweetSimWeight',''));
            this.proNounWeight= Integer.parseInt(cb.getString('experiment.proNounWeight',''));
            this.roleWeightSn= Integer.parseInt(cb.getString('experiment.roleWeightSn',''));
            this.roleWeightName= Integer.parseInt(cb.getString('experiment.roleWeightName',''));
            this.roleWeightThreat= Integer.parseInt(cb.getString('experiment.roleWeightThreat',''));
            this.roleWeightSilence= Integer.parseInt(cb.getString('experiment.roleWeightSilence',''));
            this.roleVictimToBullyWeightSilence= Integer.parseInt(cb.getString('experiment.roleVictimToBullyWeightSilence',''));
            this.roleVictimToBullyWeightThreat= Integer.parseInt(cb.getString('experiment.roleVictimToBullyWeightThreat',''));
            this.roleVictimToBullyWeightName= Integer.parseInt(cb.getString('experiment.roleVictimToBullyWeightName',''));
            this.roleVictimToBullyWeightSn= Integer.parseInt(cb.getString('experiment.roleVictimToBullyWeightSn',''));
            this.otherNeutralMentionsWeight= Integer.parseInt(cb.getString('experiment.otherNeutralMentionsWeight',''));
            this.otherNeutralWeight= Integer.parseInt(cb.getString('experiment.otherNeutralWeight',''));
            this.teasingNeutralMentionsWeight= Integer.parseInt(cb.getString('experiment.teasingNeutralMentionsWeight',''));
            this.teasingNeutralWeight= Integer.parseInt(cb.getString('experiment.teasingNeutralWeight',''));
            this.negBullyWeight= Integer.parseInt(cb.getString('experiment.negBullyWeight',''));
            this.posNeutralWeight= Integer.parseInt(cb.getString('experiment.posNeutralWeight',''));
            this.symmetricRoleWeight= Integer.parseInt(cb.getString('experiment.symmetricRoleWeight',''));
            this.followsRTieWeight= Integer.parseInt(cb.getString('experiment.followsRTieWeight',''));
            this.inferRTieWeight= Integer.parseInt(cb.getString('experiment.inferRTieWeight',''));
            this.teasingRTieWeight= Integer.parseInt(cb.getString('experiment.teasingRTieWeight',''));
            this.otherRTieWeight= Integer.parseInt(cb.getString('experiment.otherRTieWeight',''));
            this.sentRTieWeight= Integer.parseInt(cb.getString('experiment.sentRTieWeight',''));
            this.tieNotBullyWeight= Integer.parseInt(cb.getString('experiment.tieNotBullyWeight',''));
            this.tieThreatWeight= Integer.parseInt(cb.getString('experiment.tieThreatWeight',''));
            this.tieSnWeight= Integer.parseInt(cb.getString('experiment.tieSnWeight',''));
            this.tieNameWeight= Integer.parseInt(cb.getString('experiment.tieNameWeight',''));
            this.tieSilenceWeight= Integer.parseInt(cb.getString('experiment.tieSilenceWeight',''));
            this.tieOtherWeight= Integer.parseInt(cb.getString('experiment.tieOtherWeight',''));
            this.tieTeasingWeight= Integer.parseInt(cb.getString('experiment.tieTeasingWeight',''));
            this.normWeight= Integer.parseInt(cb.getString('experiment.normWeight',''));
            this.popBullyWeight= Integer.parseInt(cb.getString('experiment.popBullyWeight',''));
            this.popVictimWeight= Integer.parseInt(cb.getString('experiment.popVictimWeight',''));
            this.gangUpWeight= Integer.parseInt(cb.getString('experiment.gangUpWeight',''));
            
                    
		}
        

	}
