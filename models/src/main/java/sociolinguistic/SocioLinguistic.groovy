package sociolinguistic;

import org.linqs.psl.application.learning.weight.maxlikelihood.MaxLikelihoodMPE;
import org.linqs.psl.application.inference.MPEInference;
import org.linqs.psl.application.learning.weight.em.HardEM
import org.linqs.psl.application.learning.weight.maxlikelihood.LazyMaxLikelihoodMPE;
import org.linqs.psl.config.ConfigBundle;
import org.linqs.psl.config.ConfigManager;
import org.linqs.psl.database.Database;
import org.linqs.psl.database.DatabasePopulator;
import org.linqs.psl.database.DataStore;
import org.linqs.psl.database.Partition;
import org.linqs.psl.database.Queries;
import org.linqs.psl.database.ReadOnlyDatabase;
import org.linqs.psl.database.loading.Inserter;
import org.linqs.psl.database.rdbms.driver.H2DatabaseDriver;
import org.linqs.psl.database.rdbms.driver.H2DatabaseDriver.Type;

import org.linqs.psl.database.rdbms.driver.PostgreSQLDriver;

import org.linqs.psl.database.rdbms.RDBMSDataStore;



import org.linqs.psl.groovy.PSLModel;
import org.linqs.psl.model.atom.Atom;
import org.linqs.psl.model.predicate.StandardPredicate;
import org.linqs.psl.model.term.ConstantType;

import org.linqs.psl.database.loading.Inserter;
import org.linqs.psl.model.term.Constant;
import org.linqs.psl.model.atom.GroundAtom;
import org.linqs.psl.model.term.Constant;
import org.linqs.psl.model.term.UniqueStringID;
import org.linqs.psl.model.term.Variable;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import groovy.time.TimeCategory;
import java.nio.file.Paths;

/**
 * A BOW text classifier.  
 *
 */

public class SocioLinguistic{
	private static final String PARTITION_TRAIN_OBSERVATIONS = "trainObservations";
	private static final String PARTITION_TRAIN_TARGETS = "trainTargets";
	private static final String PARTITION_TRAIN_TRUTH = "trainTruth";

    private static final String PARTITION_TEST_OBSERVATIONS = "testObservations";
    private static final String PARTITION_TEST_TARGETS = "testTargets";
	
    
	private Logger log;
	private DataStore ds;
	private ConfigSL config;
	private PSLModel model;

    private static weightExtensionFileName;



	public SocioLinguistic(ConfigBundle cb) {
		log = LoggerFactory.getLogger(this.class);
		config = new ConfigSL(cb);
	    String suffix = System.getProperty("user.name") + "@" + getHostname();
		String baseDBPath = cb.getString("dbpath", System.getProperty("java.io.tmpdir"));
		String dbPath = Paths.get(baseDBPath, this.getClass().getName() + "" + suffix).toString();
		
        //ds = new RDBMSDataStore(new H2DatabaseDriver(Type.Disk, Paths.get(config.dbPath, 'sl').toString(), true), cb);
        
        ds = new RDBMSDataStore(new H2DatabaseDriver(Type.Disk, dbPath, true), cb);
         //ds = new RDBMSDataStore(new PostgreSQLDriver("sabina_6", true), cb);
        
		model = new PSLModel(this, ds);
	}

	/**
	 * Defines the logical predicates used in this model
	 */
	private void definePredicates() {
        
        //describe
        model.add predicate: "HasWord", types: [ConstantType.UniqueStringID, ConstantType.UniqueStringID];
        model.add predicate: "WordID", types: [ConstantType.UniqueStringID, ConstantType.String];

		model.add predicate: "BullyTweet", types: [ConstantType.UniqueStringID];
        model.add predicate: "CandidateTweet", types: [ConstantType.UniqueStringID];
        model.add predicate: "TweetSim", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
        model.add predicate: "NegTweet", types: [ConstantType.UniqueStringID];
        model.add predicate: "NeuTweet", types: [ConstantType.UniqueStringID];
        model.add predicate: "PosTweet", types: [ConstantType.UniqueStringID];
      
        model.add predicate: "SecondPersonPN", types: [ConstantType.UniqueStringID];
        model.add predicate: "ThirdPersonPN", types: [ConstantType.UniqueStringID];
      
        
        model.add predicate: "Author", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
        model.add predicate: "Mentions", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
    
        model.add predicate: "FollowsTwitter", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
        
        model.add predicate: "WordSim", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
        
        
        model.add predicate: "BadName", types: [ConstantType.UniqueStringID];
        model.add predicate: "SexistName", types: [ConstantType.UniqueStringID];
        model.add predicate: "Threat", types: [ConstantType.UniqueStringID];
        model.add predicate: "Silencing", types: [ConstantType.UniqueStringID];
        model.add predicate: "Smiley", types: [ConstantType.UniqueStringID];
        
        
        model.add predicate: "DialogAct", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
        model.add predicate: "WordInCat", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
        
        
        model.add predicate: "RoleInTweet", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID,ConstantType.UniqueStringID];
        model.add predicate: "NotEqual", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
        
        model.add predicate: "ActiveInTweet", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
        
                
 
               model.add predicate: "RoleID", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
        
        model.add predicate: "RTie", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
         model.add predicate: "CandidateRTie", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
        
        
         model.add predicate: "DAID", types: [ConstantType.UniqueStringID,ConstantType.String];
        
        model.add predicate: "WordVecSim", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
        model.add predicate: "CoOccur", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
        model.add predicate: "Associated", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
        model.add predicate: "SameCluster", types: [ConstantType.UniqueStringID,ConstantType.UniqueStringID];
        
        
         model.add predicate: "PotentialWord", types: [ConstantType.UniqueStringID];
        
         model.add predicate: "NeuUser", types: [ConstantType.UniqueStringID];
         model.add predicate: "NegUser", types: [ConstantType.UniqueStringID];
         model.add predicate: "PosUser", types: [ConstantType.UniqueStringID];
         model.add predicate: "HighPopularity", types: [ConstantType.UniqueStringID];
        
     
        
	}

	/**
	 * Defines the rules for this model
	 */
	private void defineRules() {
		log.info("Defining model rules");
        
        String nameCatID = "0"; 
        String snCatID = "1"; 
        String threatCatID ="2"; 
        String silenceCatID ="3"; 
        String teasingCatID ="4"; 
        String otherCatID ="5"; 
        
        String bullyID = "0"; 
        String victimID="1";
        String neutralID ="2"; 
          
    		model.add(
			rule:  ~BullyTweet(Tweet),
			squared: config.sqPotentials,
			weight : config.notBullyWeight
		);
       

         
         model.add(
            rule: "DialogAct(T, +X) = 1 ."

		);
 

        
        model.add(
            rule: "WordInCat(T, +X) = 1 ."

		);
        
        
         model.add(
            rule: "RoleInTweet(T, U,+X) = 1 ."

		);
 
        model.add(
			rule:  ~RTie(A,B),
			squared: config.sqPotentials,
			weight : config.notTiedWeight
		);
        
        
       //Inferring Text Categories        
       model.add(
			rule:  (CandidateTweet(Tweet)&DAID(X,otherCatID)) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.otherPrior
		);
        
        
        
        model.add(
			rule: "DialogAct(T, '5')+DialogAct(T, '4')=1-BullyTweet(T) . ", 
		);
        
  
          model.add(
			rule: "RoleInTweet(T,U, '0')=(DialogAct(T,'0')+DialogAct(T,'1')+DialogAct(T,'2')+DialogAct(T,'3')+0*Author(T,U)) ."
             
			
		);
        
          model.add(
			rule: "RoleInTweet(T,U, '1')=(DialogAct(T,'0')+DialogAct(T,'1')+DialogAct(T,'2')+DialogAct(T,'3')+0*Mentions(T,U)) . "
		);
        

         
         model.add(
			rule: (CandidateTweet(Tweet)&DialogAct(Tweet,X)&DAID(X,snCatID) ) >> BullyTweet(Tweet),
			squared: config.sqPotentials,
			weight : config.snToBullyWeight
		);
       
           model.add(
			rule: (CandidateTweet(Tweet)&DialogAct(Tweet,X)&DAID(X,silenceCatID) ) >> BullyTweet(Tweet),
			squared: config.sqPotentials,
			weight : config.silenceToBullyWeight
		);
        
          model.add(
			rule: (CandidateTweet(Tweet)&DialogAct(Tweet,X)&DAID(X,nameCatID) ) >> BullyTweet(Tweet),
			squared: config.sqPotentials,
			weight : config.nameToBullyWeight
		);
       
           model.add(
			rule: (CandidateTweet(Tweet)&DialogAct(Tweet,X)&DAID(X,threatCatID) ) >> BullyTweet(Tweet),
			squared: config.sqPotentials,
			weight : config.threatToBullyWeight
		);
        
                 
        

        
        //Words to categories
        model.add(
			rule: (Silencing(Word) &DAID(X,silenceCatID)) >> WordInCat(Word,X),
			squared: config.sqPotentials,
			weight : config.silenceToBullyWeight
		);
          
        model.add(
			rule: (Threat(Word) &DAID(X,threatCatID)) >> WordInCat(Word,X),
			squared: config.sqPotentials,
			weight : config.threatToBullyWeight
		);
          
        model.add(
			rule: (BadName(Word) &DAID(X,nameCatID)) >> WordInCat(Word,X),
			squared: config.sqPotentials,
			weight : config.nameToBullyWeight
		);
          
        model.add(
			rule: (SexistName(Word) &DAID(X,snCatID)) >> WordInCat(Word,X),
			squared: config.sqPotentials,
			weight : config.snToBullyWeight
		);
        
        model.add(
			rule: (Smiley(Word) &DAID(X,teasingCatID)) >> WordInCat(Word,X),
			squared: config.sqPotentials,
			weight : config.teasingToBullyWeight
		);
        
        

         model.add(
			rule: (CandidateTweet(Tweet)&HasWord(Tweet,Word)&Silencing(Word)&WordInCat(Word,X) ) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.wordToCatWeight
		);
        
       model.add(
			rule: (CandidateTweet(Tweet)&HasWord(Tweet,Word)&BadName(Word)&WordInCat(Word,X) ) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.wordToCatWeight
		);    
           model.add(
			rule: (CandidateTweet(Tweet)&HasWord(Tweet,Word)&SexistName(Word)&WordInCat(Word,X) ) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.wordToCatWeight
		);
           model.add(
			rule: (CandidateTweet(Tweet)&HasWord(Tweet,Word)&Threat(Word)&WordInCat(Word,X) ) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.wordToCatWeight
		);
          
         model.add(
			rule: (CandidateTweet(Tweet)&HasWord(Tweet,Word)&Smiley(Word)&WordInCat(Word,X) ) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.wordToCatWeight
		);
        
        
         //Sentiment
          model.add(
			rule: (NegTweet(Tweet)&CandidateTweet(Tweet) &DAID(X,snCatID)) >>DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.sentWeight
		);
         model.add(
			rule: (NegTweet(Tweet)&CandidateTweet(Tweet) &DAID(X,silenceCatID)) >>DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.sentWeight
		);
         model.add(
			rule: (NegTweet(Tweet)&CandidateTweet(Tweet) &DAID(X,threatCatID)) >>DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.sentWeight
		);
         model.add(
			rule: (NegTweet(Tweet)&CandidateTweet(Tweet) &DAID(X,nameCatID)) >>DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.sentWeight
		);
        
        
        
           model.add(
			rule: (NeuTweet(Tweet)&CandidateTweet(Tweet) &DAID(X,otherCatID)) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.sentWeight
		);
        
         model.add(
			rule: (PosTweet(Tweet)&CandidateTweet(Tweet) &DAID(X,otherCatID)) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.sentWeight
		);
       
  
       
    
        //Word associations
      model.add(
			rule: (WordInCat(Word_one,X)&PotentialWord(Word_two)&WordVecSim(Word_one,Word_two) ) >> WordInCat(Word_two,X),
			squared: config.sqPotentials,
			weight : config.wvSimWeight
		);    
        
        
              model.add(
			rule: (WordInCat(Word_one,X)&PotentialWord(Word_two)&CoOccur(Word_one,Word_two) ) >> WordInCat(Word_two,X),
			squared: config.sqPotentials,
			weight : config.cooccurWeight
		);  
        
        
              model.add(
			rule: (WordInCat(Word_one,X)&PotentialWord(Word_two)&Associated(Word_one,Word_two) ) >> WordInCat(Word_two,X),
			squared: config.sqPotentials,
			weight : config.assocWeight
		);  
        
              model.add(
			rule: (WordInCat(Word_one,X)&PotentialWord(Word_two)&SameCluster(Word_one,Word_two) ) >> WordInCat(Word_two,X),
			squared: config.sqPotentials,
			weight : config.sClusterWeight
		);  
        
        
     
         model.add(
			rule: (HasWord(Tweet,Word)&PotentialWord(Word)&WordInCat(Word,X) ) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.potentialWordToCatWeight
		);
        
      model.add(
			rule: (DialogAct(Tweet_one,X)&TweetSim(Tweet_one,Tweet_two) ) >> DialogAct(Tweet_two,X),
			squared: config.sqPotentials,
			weight : config.tweetSimWeight
		);
        

 
       
        
   
        
        
        
   
        //Subjects and Text Categories
                model.add(
			rule: (HasWord(Tweet,Word_one)&SecondPersonPN(Word_one)&HasWord(Tweet,Word_two)&WordInCat(Word_two,X)&DAID(X,threatCatID)) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.proNounWeight
		);
        
        
               model.add(
			rule: (HasWord(Tweet,Word_one)&SecondPersonPN(Word_one)&HasWord(Tweet,Word_two)&WordInCat(Word_two,X)&DAID(X,nameCatID)) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.proNounWeight
		);
        
          model.add(
			rule: (HasWord(Tweet,Word_one)&SecondPersonPN(Word_one)&HasWord(Tweet,Word_two)&WordInCat(Word_two,X)&DAID(X,snCatID)) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.proNounWeight
		);
        
         model.add(
			rule: (HasWord(Tweet,Word_one)&SecondPersonPN(Word_one)&HasWord(Tweet,Word_two)&WordInCat(Word_two,X)&DAID(X,silenceCatID)) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.proNounWeight
		);
        
        
        
         model.add(
			rule: (HasWord(Tweet,Word_one)&ThirdPersonPN(Word_one)&HasWord(Tweet,Word_two)&WordInCat(Word_two,X)&DAID(X,snCatID)&DAID(Y,otherCatID)) >> DialogAct(Tweet,Y),
			squared: config.sqPotentials,
			weight : config.proNounWeight
		);
        
        
        model.add(
			rule: (HasWord(Tweet,Word_one)&ThirdPersonPN(Word_one)&HasWord(Tweet,Word_two)&WordInCat(Word_two,X)&DAID(X,nameCatID)&DAID(Y,otherCatID)) >> DialogAct(Tweet,Y),
			squared: config.sqPotentials,
			weight : config.proNounWeight
		);
            model.add(
			rule: (HasWord(Tweet,Word_one)&ThirdPersonPN(Word_one)&HasWord(Tweet,Word_two)&WordInCat(Word_two,X)&DAID(X,threatCatID)&DAID(Y,otherCatID)) >> DialogAct(Tweet,Y),
			squared: config.sqPotentials,
			weight : config.proNounWeight
		);
        
        model.add(
			rule: (HasWord(Tweet,Word_one)&ThirdPersonPN(Word_one)&HasWord(Tweet,Word_two)&WordInCat(Word_two,X)&DAID(X,silenceCatID)&DAID(Y,otherCatID)) >> DialogAct(Tweet,Y),
			squared: config.sqPotentials,
			weight : config.proNounWeight
		);
        
        
           //User Roles
         model.add(
			rule: (DialogAct(Tweet,X)&(DAID(X,snCatID)) & Author(Tweet,User)&RoleID(Y,bullyID)&CandidateTweet(Tweet)) >> RoleInTweet(Tweet,User,Y) ,
			squared: config.sqPotentials,
			weight : config.roleWeightSn
		      );
       
       
        model.add(
			rule: (DialogAct(Tweet,X)&(DAID(X,nameCatID)) & Author(Tweet,User)&RoleID(Y,bullyID)&CandidateTweet(Tweet)) >> RoleInTweet(Tweet,User,Y) ,
			squared: config.sqPotentials,
			weight : config.roleWeightName
		      );
        
        model.add(
			rule: (DialogAct(Tweet,X)&(DAID(X,threatCatID)) & Author(Tweet,User)&RoleID(Y,bullyID)&CandidateTweet(Tweet)) >> RoleInTweet(Tweet,User,Y) ,
			squared: config.sqPotentials,
			weight : config.roleWeightThreat
		      );
        
        model.add(
			rule: (DialogAct(Tweet,X)&(DAID(X,silenceCatID)) & Author(Tweet,User)&RoleID(Y,bullyID)&CandidateTweet(Tweet)) >> RoleInTweet(Tweet,User,Y) ,
			squared: config.sqPotentials,
			weight : config.roleWeightSilence
		      );
        
                 model.add(
			rule: (DialogAct(Tweet,X)&(DAID(X,silenceCatID)) & Mentions(Tweet,User)&RoleID(Y,victimID)&CandidateTweet(Tweet)) >> RoleInTweet(Tweet,User,Y) ,
			squared: config.sqPotentials,
			weight : config.roleVictimToBullyWeightSilence
		      );
         model.add(
			rule: (DialogAct(Tweet,X)&(DAID(X,threatCatID)) & Mentions(Tweet,User)&RoleID(Y,victimID)&CandidateTweet(Tweet)) >> RoleInTweet(Tweet,User,Y) ,
			squared: config.sqPotentials,
			weight : config.roleVictimToBullyWeightThreat
		      );
         model.add(
			rule: (DialogAct(Tweet,X)&(DAID(X,nameCatID)) & Mentions(Tweet,User)&RoleID(Y,victimID)&CandidateTweet(Tweet)) >> RoleInTweet(Tweet,User,Y) ,
			squared: config.sqPotentials,
			weight : config.roleVictimToBullyWeightName
		      );
         model.add(
			rule: (DialogAct(Tweet,X)&(DAID(X,snCatID)) & Mentions(Tweet,User)&RoleID(Y,victimID)&CandidateTweet(Tweet)) >> RoleInTweet(Tweet,User,Y) ,
			squared: config.sqPotentials,
			weight : config.roleVictimToBullyWeightSn
		      );
        

          model.add(
			rule: (DialogAct(Tweet,X)&DAID(X,otherCatID) & Mentions(Tweet,User)&RoleID(Y,neutralID)&CandidateTweet(Tweet)) >> RoleInTweet(Tweet,User,Y) ,
			squared: config.sqPotentials,
			weight :  config.otherNeutralMentionsWeight

		      );
        
        model.add(
			rule: ( DialogAct(Tweet,X)&DAID(X,otherCatID)& Author(Tweet,User)&RoleID(Y,neutralID)&CandidateTweet(Tweet)) >> RoleInTweet(Tweet,User,Y) ,
			squared: config.sqPotentials,
			weight : config.otherNeutralWeight
      
		      );
        
             model.add(
			rule: (DialogAct(Tweet,X)&DAID(X,teasingCatID) & Mentions(Tweet,User)&RoleID(Y,neutralID)&CandidateTweet(Tweet)) >> RoleInTweet(Tweet,User,Y) ,
			squared: config.sqPotentials,
			weight : config.teasingNeutralMentionsWeight

		      );
        
        model.add(
			rule: ( DialogAct(Tweet,X)&DAID(X,teasingCatID)& Author(Tweet,User)&RoleID(Y,neutralID)&CandidateTweet(Tweet)) >> RoleInTweet(Tweet,User,Y) ,
			squared: config.sqPotentials,
			weight : config.teasingNeutralWeight

		      );
        
     
        
          model.add(
			rule: (Author(Tweet,User)&NegUser(User)&RoleID(X,bullyID) ) >> RoleInTweet(Tweet,User,X),
			squared: config.sqPotentials,
			weight : config.negBullyWeight
     
		);
        
          model.add(
			rule: (Author(Tweet,User)&PosUser(User)&RoleID(X,neutralID) ) >> RoleInTweet(Tweet,User,X),
			squared: config.sqPotentials,
			weight : config.posNeutralWeight
   
		);
    
        

        
        model.add(
			rule: ( Author(Tweet,User)&RoleID(Y,bullyID)&RoleID(X,victimID)&Mentions(Tweet,User_two)&RoleInTweet(Tweet,User,Y)) >> RoleInTweet(Tweet,User_two,X) ,
			squared: config.sqPotentials,
			weight : config.symmetricRoleWeight
		      );
        
        //Inferring Relational Ties
        
          model.add(
			rule: ( FollowsTwitter(U_a,U_b) ) >> RTie(U_a,U_b),
			squared: config.sqPotentials,
			weight : config.followsRTieWeight
		      );

        model.add(
			rule: (RTie(U_a,U_b) & RTie(U_b,U_c) &NotEqual(U_a,U_b) ) >> RTie(U_a,U_c),
			squared: config.sqPotentials,
			weight : config.inferRTieWeight
		      );
      
           model.add(
			rule: (Author(Tweet,U_a) & Mentions(Tweet,U_b)& DialogAct(Tweet,X)&DAID(X,teasingCatID)) >> RTie(U_a,U_b),
			squared: config.sqPotentials,
			weight : config.teasingRTieWeight
		      );
        
        model.add(
			rule: (Author(Tweet,U_a) & Mentions(Tweet,U_b)& DialogAct(Tweet,X)&DAID(X,otherCatID)) >> RTie(U_a,U_b),
			squared: config.sqPotentials,
			weight : config.otherRTieWeight
		      );
        
        model.add(
			rule: (Author(Tweet,U_a)& Mentions(Tweet,U_b)& PosTweet(Tweet)) >> RTie(U_a,U_b),
			squared: config.sqPotentials,
			weight : config.sentRTieWeight
		      );
        
          model.add(
			rule: (Author(Tweet,U_a) & Mentions(Tweet,U_b)& NeuTweet(Tweet)) >> RTie(U_a,U_b),
			squared: config.sqPotentials,
			weight : config.sentRTieWeight
		      );
        
        
     //Ties and Conversation
      model.add(
			rule: (Mentions(Tweet,User) &Author(Tweet,User_two)&RTie(User,User_two)&DAID(X,teasingCatID)&HasWord(Tweet,Word_two)&WordInCat(Word_two,Y)&DAID(Y,snCatID)&HasWord(Tweet,Word)&WordInCat(Word,X) ) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.tieNotBullyWeight
		);
 model.add(
			rule: (Mentions(Tweet,User) &Author(Tweet,User_two)&RTie(User,User_two)&DAID(X,teasingCatID)&HasWord(Tweet,Word_two)&WordInCat(Word_two,Y)&DAID(Y,nameCatID)&HasWord(Tweet,Word)&WordInCat(Word,X) ) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.tieNotBullyWeight
		);
 model.add(
			rule: (Mentions(Tweet,User) &Author(Tweet,User_two)&RTie(User,User_two)&DAID(X,teasingCatID)&HasWord(Tweet,Word_two)&WordInCat(Word_two,Y)&DAID(Y,threatCatID)&HasWord(Tweet,Word)&WordInCat(Word,X) ) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.tieNotBullyWeight
		);
 model.add(
			rule: (Mentions(Tweet,User) &Author(Tweet,User_two)&RTie(User,User_two)&DAID(X,teasingCatID)&HasWord(Tweet,Word_two)&WordInCat(Word_two,Y)&DAID(Y,silenceCatID) &HasWord(Tweet,Word)&WordInCat(Word,X)) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.tieNotBullyWeight
		);       
        
        
          model.add(
			rule: (Mentions(Tweet,User) &Author(Tweet,User_two)&RTie(User,User_two)&DAID(X,threatCatID) ) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.tieThreatWeight
		);
        
         model.add(
			rule: (Mentions(Tweet,User) &Author(Tweet,User_two)&RTie(User,User_two)&DAID(X,snCatID) ) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.tieSnWeight
		);
        
         model.add(
			rule: (Mentions(Tweet,User) &Author(Tweet,User_two)&RTie(User,User_two)&DAID(X,nameCatID) ) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.tieNameWeight
		);
        
         model.add(
			rule: (Mentions(Tweet,User) &Author(Tweet,User_two)&RTie(User,User_two)&DAID(X,silenceCatID) ) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.tieSilenceWeight
		);
        
         model.add(
			rule: (Mentions(Tweet,User) &Author(Tweet,User_two)&RTie(User,User_two)&DAID(X,otherCatID) ) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.tieOtherWeight
		);
        
         model.add(
			rule: (Mentions(Tweet,User) &Author(Tweet,User_two)&RTie(User,User_two)&DAID(X,teasingCatID) ) >> DialogAct(Tweet,X),
			squared: config.sqPotentials,
			weight : config.tieTeasingWeight
		);
        
     //Social Behavior    
     model.add(
			rule: ( RTie(U_a,U_b)& Author(Tweet_one,U_a)&RoleInTweet(Tweet_one,U_a,X)&Author(Tweet_two,U_b)) >> RoleInTweet(Tweet_two,U_b,X),
			squared: config.sqPotentials,
			weight : config.normWeight
		      );

         model.add(
			rule: ( RTie(U_a,U_b)& Mentions(Tweet_one,U_a)&RoleInTweet(Tweet_one,U_a,X)&Mentions(Tweet_two,U_b)) >> RoleInTweet(Tweet_two,U_b,X),
			squared: config.sqPotentials,
			weight : config.normWeight
		      );
        
       model.add(
			rule: (Author(Tweet,User)&HighPopularity(User)&RoleID(X,bullyID) ) >> RoleInTweet(Tweet,User,X),
			squared: config.sqPotentials,
			weight : config.popBullyWeight
		);
      
        model.add(
			rule: (Mentions(Tweet,User)&HighPopularity(User)&RoleID(X,victimID) ) >> ~RoleInTweet(Tweet,User,X),
			squared: config.sqPotentials,
			weight : config.popVictimWeight
		);    
        
        
         model.add(
			rule: ( RTie(U_a,U_b)& Author(Tweet_one,U_a)&Mentions(Tweet_one,U_c)&RoleInTweet(Tweet_one,U_c,Y)&Author(Tweet_two,U_b)&Mentions(Tweet_two,U_c)&RoleID(Y,victimID)) >> RoleInTweet(Tweet_two,U_c,Y),
			squared: config.sqPotentials,
			weight : config.gangUpWeight
		      );
        
    
		log.debug("model: {}", model);
	}

	/**
	 * Load data from text files into the DataStore. Three partitions are defined
	 * and populated: observations, targets, and truth.
	 * Observations contains evidence that we treat as background knowledge and
	 * use to condition our inferences
	 * Targets contains the inference targets - the unknown variables we wish to infer
	 * Truth contains the true values of the inference variables and will be used
	 * to evaluate the model's performance
	 */
	private void loadTrainData(Partition obsPartition, Partition targetsPartition, Partition truthPartition) {
		log.info("Loading data into database");

        
        //First we load all of the observed data
        
		Inserter inserter = ds.getInserter(HasWord, obsPartition);
		inserter.loadDelimitedData(Paths.get(config.dataPath, "HasWord.txt").toString());
        
        inserter = ds.getInserter(WordID, obsPartition);
		inserter.loadDelimitedData(Paths.get(config.dataPath, "WordID.txt").toString());
        
        
        
        inserter = ds.getInserter(SecondPersonPN, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "SecondPronoun.txt").toString());
        
        inserter = ds.getInserter(ThirdPersonPN, obsPartition);
		inserter.loadDelimitedData(Paths.get(config.dataPath, "ThirdPronoun.txt").toString());
        
      
      
               

        inserter = ds.getInserter(Author, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "Author.txt").toString());
        
        
        inserter = ds.getInserter(Mentions, obsPartition);
		inserter.loadDelimitedData(Paths.get(config.dataPath, "TweetMentions.txt").toString());
        
         inserter = ds.getInserter(RoleID, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "RoleID.txt").toString());
        
       
        
        inserter = ds.getInserter(FollowsTwitter, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "FollowsTwitter.txt").toString());


        inserter = ds.getInserter(DAID, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "DAID.txt").toString());
        
        inserter = ds.getInserter(TweetSim, obsPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "TweetSims.txt").toString());
        
        
        inserter = ds.getInserter(NegTweet, obsPartition);
		inserter.loadDelimitedDataTruth(Paths.get(config.dataPath, "NegTweet.txt").toString());
        
        inserter = ds.getInserter(NeuTweet, obsPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "NeuTweet.txt").toString());
        
        inserter = ds.getInserter(PosTweet, obsPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "PosTweet.txt").toString());
  
         inserter = ds.getInserter(NeuUser, obsPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "NeuUser.txt").toString());
            
         inserter = ds.getInserter(PosUser, obsPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "PosUser.txt").toString());
            
         inserter = ds.getInserter(NegUser, obsPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "NegUser.txt").toString());  
        
              
           inserter = ds.getInserter(Silencing, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "Silencing.txt").toString());    
            
            
            inserter = ds.getInserter(Threat, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "Threat.txt").toString());    
            
            
            inserter = ds.getInserter(BadName, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "BadName.txt").toString());    
            
        inserter = ds.getInserter(SexistName, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "SName.txt").toString());    
            
         inserter = ds.getInserter(Smiley, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "Smileys.txt").toString());
        
        inserter = ds.getInserter(WordVecSim, obsPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "WordVecSim.txt").toString());
        
        inserter = ds.getInserter(CoOccur, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "CoOccurs.txt").toString());
        
        inserter = ds.getInserter(Associated, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "Associated.txt").toString());
        
        inserter = ds.getInserter(SameCluster, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "SameCluster.txt").toString());
        
        //These are all of the things whoses values you would like to infer
        
        //Target variable
        inserter = ds.getInserter(BullyTweet, targetsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "Bully_target_train.txt").toString());
        
        //Latent variables
        inserter = ds.getInserter(DialogAct, targetsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "DialogActs.txt").toString());
        
        inserter = ds.getInserter(WordInCat, targetsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "WordInCat.txt").toString());

      
        inserter = ds.getInserter(RoleInTweet, targetsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "TweetRole.txt").toString());  
        
        inserter = ds.getInserter(RTie, targetsPartition);
		inserter.loadDelimitedData(Paths.get(config.dataPath, "RTie.txt").toString());
        
         //Ground truth labels 
        inserter = ds.getInserter(BullyTweet, truthPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "Bully_label_train.txt").toString());
        
      
        
        
        
	}

    	private void loadTestData(Partition obsPartition, Partition targetsPartition) {
		log.info("Loading data into database");

	   
        
        //First we load all of the observed data
        
		Inserter inserter = ds.getInserter(HasWord, obsPartition);
		inserter.loadDelimitedData(Paths.get(config.dataPath, "HasWord.txt").toString());
        
        inserter = ds.getInserter(WordID, obsPartition);
		inserter.loadDelimitedData(Paths.get(config.dataPath, "WordID.txt").toString());
        
        
        
        inserter = ds.getInserter(SecondPersonPN, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "SecondPronoun.txt").toString());
        
        inserter = ds.getInserter(ThirdPersonPN, obsPartition);
		inserter.loadDelimitedData(Paths.get(config.dataPath, "ThirdPronoun.txt").toString());
        
      
      
               

        inserter = ds.getInserter(Author, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "Author.txt").toString());
        
        
        inserter = ds.getInserter(Mentions, obsPartition);
		inserter.loadDelimitedData(Paths.get(config.dataPath, "TweetMentions.txt").toString());
        
         inserter = ds.getInserter(RoleID, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "RoleID.txt").toString());
        
       
        
        inserter = ds.getInserter(FollowsTwitter, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "FollowsTwitter.txt").toString());


        inserter = ds.getInserter(DAID, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "DAID.txt").toString());
        
        inserter = ds.getInserter(TweetSim, obsPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "TweetSims.txt").toString());
        
        
        inserter = ds.getInserter(NegTweet, obsPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "NegTweet.txt").toString());
        
        inserter = ds.getInserter(NeuTweet, obsPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "NeuTweet.txt").toString());
        
        inserter = ds.getInserter(PosTweet, obsPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "PosTweet.txt").toString());
  
         inserter = ds.getInserter(NeuUser, obsPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "NeuUser.txt").toString());
            
         inserter = ds.getInserter(PosUser, obsPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "PosUser.txt").toString());
            
         inserter = ds.getInserter(NegUser, obsPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "NegUser.txt").toString());  
        
              
           inserter = ds.getInserter(Silencing, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "Silencing.txt").toString());    
            
            
            inserter = ds.getInserter(Threat, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "Threat.txt").toString());    
            
            
            inserter = ds.getInserter(BadName, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "BadName.txt").toString());    
            
        inserter = ds.getInserter(SexistName, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "SName.txt").toString());    
            
         inserter = ds.getInserter(Smiley, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "Smileys.txt").toString());
        
        inserter = ds.getInserter(WordVecSim, obsPartition);
		inserter.loadDelimitedDataTruth( Paths.get(config.dataPath, "WordVecSim.txt").toString());
        
        inserter = ds.getInserter(CoOccur, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "CoOccurs.txt").toString());
        
        inserter = ds.getInserter(Associated, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "Associated.txt").toString());
        
        inserter = ds.getInserter(SameCluster, obsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "SameCluster.txt").toString());
        
        //These are all of the things whoses values you would like to infer
        
        //Target variable
        inserter = ds.getInserter(BullyTweet, targetsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "Bully_target_test.txt").toString());
        
        //Latent variables
        inserter = ds.getInserter(DialogAct, targetsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "DialogActs.txt").toString());
        
        inserter = ds.getInserter(WordInCat, targetsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "WordInCat.txt").toString());

      
        inserter = ds.getInserter(RoleInTweet, targetsPartition);
		inserter.loadDelimitedData( Paths.get(config.dataPath, "TweetRole.txt").toString());  
        
        inserter = ds.getInserter(RTie, targetsPartition);
		inserter.loadDelimitedData(Paths.get(config.dataPath, "RTie.txt").toString());
        
     
      
      

	
	}
    
    //Weight learning
    //The EM method can be changed if you want, you can look in PSL code for more details
    private void learnWeights(Partition obsPartition, Partition truthPartition, Partition targetPartition)
    {
       
        HashSet closed = new HashSet<StandardPredicate>([HasWord,WordID,TweetSim,CandidateTweet,NegTweet,NeuTweet,PosTweet,SecondPersonPN,ThirdPersonPN,WordVecSim,PotentialWord,Author,Mentions,DAID,FollowsTwitter,SexistName,BadName,Threat,Silencing,CoOccur,Associated,SameCluster,RoleID,CandidateRTie,ActiveInTweet,PosUser,NeuUser,NegUser,HighPopularity]);
		
        Database observedDB = ds.getDatabase(targetPartition, closed, obsPartition);
        
        HashSet closedTwo = new HashSet<StandardPredicate>([BullyTweet]);
		
        
        Database trueDB = ds.getDatabase(truthPartition, closedTwo);
        
       
        HardEM weightLearning = new HardEM(model, observedDB, trueDB, config.cb);
        weightLearning.learn();
        weightLearning.close();
        observedDB.close();
        trueDB.close();
    }
    
	/**
	 * Run inference to infer the unknown Knows relationships between people.
	 */
	private void runInference(Partition obsPartition, Partition targetsPartition) {
		log.info("Starting inference");

		Date infStart = new Date();
		HashSet closed = new HashSet<StandardPredicate>([HasWord,WordID,TweetSim,CandidateTweet,NegTweet,NeuTweet,PosTweet,SecondPersonPN,ThirdPersonPN,WordVecSim,PotentialWord,Author,Mentions,DAID,FollowsTwitter,SexistName,BadName,Threat,Silencing,CoOccur,Associated,SameCluster,RoleID,CandidateRTie,ActiveInTweet,PosUser,NeuUser,NegUser,HighPopularity]);
		Database inferDB = ds.getDatabase(targetsPartition, closed, obsPartition);
		MPEInference inferenceApp = new MPEInference(model, inferDB, config.cb);

		inferenceApp.mpeInference();
		inferenceApp.close();
		inferDB.close();

		log.info("Finished inference in {}", TimeCategory.minus(new Date(), infStart));
	}

	/**
	 * Writes the output of the model into several files
     * We write one file for the target variables and one for each latent variable 
	 */
	private void writeOutput(Partition targetsPartition) {
        
        String rootName = config.trainCondition+"_"+config.maybeCondition+"_sociolinguistic_results.txt";
		Database resultsDB = ds.getDatabase(targetsPartition);
        
       
        
      
          FileWriter writer = new  FileWriter(Paths.get(config.outputPath, "predictions_"+rootName).toString());

		for (GroundAtom atom : Queries.getAllAtoms(resultsDB, BullyTweet)) {
			for (Constant argument : atom.getArguments()) {
				writer.write(argument.toString() + "\t");
			}
			writer.write("" + atom.getValue() + "\n");
		}
        
      
         writer = new FileWriter(Paths.get(config.outputPath, "wordCategory_"+rootName).toString());

		for (GroundAtom atom : Queries.getAllAtoms(resultsDB, WordInCat)) {
			for (Constant argument : atom.getArguments()) {
				writer.write(argument.toString() + "\t");
			}
			writer.write("" + atom.getValue() + "\n");
		}
        
   
        
 writer = new FileWriter(Paths.get(config.outputPath, "textCategory_"+rootName).toString());

		for (GroundAtom atom : Queries.getAllAtoms(resultsDB, DialogAct)) {
			for (Constant argument : atom.getArguments()) {
				writer.write(argument.toString() + "\t");
			}
			writer.write("" + atom.getValue() + "\n");
		}
        
   writer = new FileWriter(Paths.get(config.outputPath, "role_"+rootName).toString());

		for (GroundAtom atom : Queries.getAllAtoms(resultsDB, RoleInTweet)) {
			for (Constant argument : atom.getArguments()) {
				writer.write(argument.toString() + "\t");
			}
			writer.write("" + atom.getValue() + "\n");
		}

        
        writer = new FileWriter(Paths.get(config.outputPath, "RTie_"+rootName).toString());

		for (GroundAtom atom : Queries.getAllAtoms(resultsDB, RTie)) {
			for (Constant argument : atom.getArguments()) {
				writer.write(argument.toString() + "\t");
			}
			writer.write("" + atom.getValue() + "\n");
		}
           
		resultsDB.close();
	}

	/*
Calls all functions to run the model. Data loading, weight learning, inference and saving output. 
*/

	public void run() {
		log.info("Running experiment {}", config.experimentName);

		Partition obsTrainPartition = ds.getPartition(PARTITION_TRAIN_OBSERVATIONS);
		Partition targetsTrainPartition = ds.getPartition(PARTITION_TRAIN_TARGETS);
		Partition truthTrainPartition = ds.getPartition(PARTITION_TRAIN_TRUTH);

        Partition obsTestPartition = ds.getPartition(PARTITION_TEST_OBSERVATIONS);
		Partition targetsTestPartition = ds.getPartition(PARTITION_TEST_TARGETS);

		definePredicates();
		defineRules();
		loadTrainData(obsTrainPartition, targetsTrainPartition, truthTrainPartition);
        learnWeights(obsTrainPartition,truthTrainPartition,targetsTrainPartition);
        loadTestData(obsTestPartition, targetsTestPartition);
        
       
		runInference(obsTestPartition, targetsTestPartition);
		writeOutput(targetsTestPartition);
		
		ds.close();
	}

	/**
	 * Parse the command line options and populate them into a ConfigBundle
	 * Currently the only argument supported is the path to the data directory
	 * @param args - the command line arguments provided during the invocation
	 * @return - a ConfigBundle populated with options from the command line options
	 */
	public static ConfigBundle populateConfigBundle(String[] args) {
		ConfigBundle cb = ConfigManager.getManager().getBundle("bully");
		if (args.length > 0) {
			
            
            cb.setProperty('experiment.trainCondition', args[0]);
            cb.setProperty('experiment.maybeCondition', args[1]);
            cb.setProperty('experiment.notBullyWeight', args[2]);
            cb.setProperty('experiment.notTiedWeight', args[3]);
            cb.setProperty('experiment.otherPrior', args[4]);
            cb.setProperty('experiment.snToBullyWeight', args[5]);
            cb.setProperty('experiment.silenceToBullyWeight', args[6]);
            cb.setProperty('experiment.nameToBullyWeight', args[7]);
            cb.setProperty('experiment.threatToBullyWeight', args[8]);
            cb.setProperty('experiment.snToBullyWeight', args[9]);
            cb.setProperty('experiment.teasingToBullyWeight', args[10]);
            cb.setProperty('experiment.wordToCatWeight', args[11]);
            cb.setProperty('experiment.sentWeight', args[12]);
            cb.setProperty('experiment.wvSimWeight', args[13]);
            cb.setProperty('experiment.cooccurWeight', args[14]);
            cb.setProperty('experiment.assocWeight', args[15]);
            cb.setProperty('experiment.sClusterWeight', args[16]);
            cb.setProperty('experiment.potentialWordToCatWeight', args[17]);
            cb.setProperty('experiment.tweetSimWeight', args[18]);
            cb.setProperty('experiment.proNounWeight', args[19]);
            cb.setProperty('experiment.roleWeightSn', args[20]);
            cb.setProperty('experiment.roleWeightName', args[21]);
            cb.setProperty('experiment.roleWeightThreat', args[22]);
            cb.setProperty('experiment.roleWeightSilence', args[23]);
            cb.setProperty('experiment.roleVictimToBullyWeightSilence', args[24]);
            cb.setProperty('experiment.roleVictimToBullyWeightThreat', args[25]);
            cb.setProperty('experiment.roleVictimToBullyWeightName', args[26]);
            cb.setProperty('experiment.roleVictimToBullyWeightSn', args[27]);
            cb.setProperty('experiment.otherNeutralMentionsWeight', args[28]);
            cb.setProperty('experiment.otherNeutralWeight', args[29]);
            cb.setProperty('experiment.teasingNeutralMentionsWeight', args[30]);
            cb.setProperty('experiment.teasingNeutralWeight', args[31]);
            cb.setProperty('experiment.negBullyWeight', args[32]);
            cb.setProperty('experiment.posNeutralWeight', args[33]);
            cb.setProperty('experiment.symmetricRoleWeight', args[34]);
            cb.setProperty('experiment.followsRTieWeight', args[35]);
            cb.setProperty('experiment.inferRTieWeight', args[36]);
            cb.setProperty('experiment.teasingRTieWeight', args[37]);
            cb.setProperty('experiment.otherRTieWeight', args[38]);
            cb.setProperty('experiment.sentRTieWeight', args[39]);
            cb.setProperty('experiment.tieNotBullyWeight', args[40]);
            cb.setProperty('experiment.tieThreatWeight', args[41]);
            cb.setProperty('experiment.tieSnWeight', args[42]);
            cb.setProperty('experiment.tieNameWeight', args[43]);
            cb.setProperty('experiment.tieSilenceWeight', args[44]);
            cb.setProperty('experiment.tieOtherWeight', args[45]);
            cb.setProperty('experiment.tieTeasingWeight', args[46]);
            cb.setProperty('experiment.normWeight', args[47]);
            cb.setProperty('experiment.popBullyWeight', args[48]);
            cb.setProperty('experiment.popVictimWeight', args[49]);
            cb.setProperty('experiment.gangUpWeight', args[50]);        
            
		}
		return cb;
	}

    
    private static String getHostname() {
		String hostname = "unknown";

		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException ex) {
			System.out.println("Hostname can not be resolved, using '" + hostname + "'.");
		}

		return hostname;
	}
    
    
	/**
	 * Run this model from the command line
	 * @param args - the command line arguments
	 */
	public static void main(String[] args) {


        
		ConfigBundle configBundle = populateConfigBundle(args);
		SocioLinguistic infer_bully = new SocioLinguistic(configBundle);
		infer_bully.run();
       
	}
}
