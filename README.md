This project includes the code needed to reproduce the results in the paper: 
"A Socio-linguistic Model for Cyberbullying Detection". If using this code please cite the paper using the following bibtex: 

```tex
@InProceedings{tomkins:asonam18,
  author       = "Tomkins, Sabina and Getoor, Lise and Chen, Yunfei and Zhang, Yi",
  title        = "A Socio-Linguistic Model for Cyberbullying Detection",
  booktitle    = "Advances in Social Networks Analysis and Mining (ASONAM)",
  year         = "2018",
}
```






The models included in the paper are in the [models](/models) directory. We include sample data in the [data](data) directory.


